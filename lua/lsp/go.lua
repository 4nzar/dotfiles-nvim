-- -------------------------------------------------------------------------- --
--                                                                            --
--       _   ___   ___   ___   ___   ___   ___   ___   ___   ___   ___        --
--       _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _|       --
--      |  _  |  _  |_   _|  _  |  _  |  _  |_   _|  _  |  _  |  _  |_        --
--      |_| |_| | |___| |___| | |_| |_| | |___| |___| | |_| |_| | |___|       --
--       _   _  |  ___   ___  |  _   _  |_   _____   _|  _   _  |  ___        --
--      | |_| | |_|  _| |_  |_| | |_| |  _| |_   _| |_  | |_| | |_|  _|       --
--      |_   _|  _  |_   _|  _  |_   _| |  _  | |  _  | |_   _|  _  |_        --
--       _| |___| |___| |___| |___| |_  |_| |_| |_| |_|  _| |___| |___|       --
--                                                                            --
--   go.lua                                                                   --
--                                                                            --
--   By: 4nzar <sadik.essaidi@gmail.com>                                      --
--                                                                            --
--   Created: 15/03/2023 14:28:20 by 4nzar                                    --
--   Updated: 15/03/2023 14:28:20 by 4nzar                                    --
--                                                                            --
-- -------------------------------------------------------------------------- --

-- START
local lsp = require("config.lsp")
local nvim_lsp = require("lspconfig")

nvim_lsp.gopls.setup{
	cmd = {'gopls'},
	-- for postfix snippets and analyzers
	capabilities = lsp.capabilities,
	settings = {
		gopls = {
			experimentalPostfixCompletions = true,
			analyses = {
				unusedparams = true,
				shadow= true,
			},
			staticcheck = true,
		}
	},
	on_attach = lsp.on_attach
}

function goimports(timeoutms)
	local context = { source = { organizeImports = true } } 
	vim.validate { context = { context, "t", true } }

	local params = vim.lsp.util.make_range_params()
	params.context = context

	-- See the implementation of the textDocument/codeAction callback
	-- (lua/vim/lsp/handler.lua) for how to do this properly.
	local result = vim.lsp.buf_request_sync(0, "textDocument/codeAction", params, timeout_ms)
	if not result or next(result) == nil then return end
	local actions = result[1].result
	if not actions then return end
	local action = actions[1]

	-- textDocument/codeAction can return either Command[] or CodeAction[]. If it
	-- is a CodeAction, it can have either an edit, a command or both. Edits
	-- should be executed first.
	if action.edit or type(action.command) == "table" then
		if action.edit then
			vim.lsp.util.apply_workspace_edit(action.edit) 
		end
		if type(action.command) == "table" then
			vim.lsp.buf.execute_command(action.command)
		end
	else
		vim.lsp.buf.execute_command(action)
	end
end

vim.api.nvim_create_autocmd({ "BufWritePre" }, { pattern = {"*.go"}, command = "lua vim.lsp.buf.formatting()" })
vim.api.nvim_create_autocmd({ "BufWritePre" }, { pattern = {"*.go"}, command = "lua goimports(1000)" })
-- END

