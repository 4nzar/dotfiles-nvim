-- -------------------------------------------------------------------------- --
--                                                                            --
--       _   ___   ___   ___   ___   ___   ___   ___   ___   ___   ___        --
--       _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _|       --
--      |  _  |  _  |_   _|  _  |  _  |  _  |_   _|  _  |  _  |  _  |_        --
--      |_| |_| | |___| |___| | |_| |_| | |___| |___| | |_| |_| | |___|       --
--       _   _  |  ___   ___  |  _   _  |_   _____   _|  _   _  |  ___        --
--      | |_| | |_|  _| |_  |_| | |_| |  _| |_   _| |_  | |_| | |_|  _|       --
--      |_   _|  _  |_   _|  _  |_   _| |  _  | |  _  | |_   _|  _  |_        --
--       _| |___| |___| |___| |___| |_  |_| |_| |_| |_|  _| |___| |___|       --
--                                                                            --
--   settings.lua                                                             --
--                                                                            --
--   By: 4nzar <sadik.essaidi@proton.me>                                      --
--                                                                            --
--   Created: 03/03/2023 12:22:58 by 4nzar                                    --
--   Updated: 03/03/2023 12:22:58 by 4nzar                                    --
--                                                                            --
-- -------------------------------------------------------------------------- --

-- START

--
local set = vim.opt
local cmd = vim.cmd

--
set.cursorline = true                         -- highlight current line
set.cursorcolumn = true                       -- highlight current line
set.number = true                             -- show line numbers
set.tabstop = 4                               -- 
set.shiftwidth = 4                            -- size of indent
set.expandtab = false                         -- spaces instead of tabs
set.mouse = ""                                -- disable mouse
set.splitbelow = true                         -- put new windows below current
set.splitright = true                         -- put new vertical splits to right
set.termguicolors = true                      -- truecolor support
set.smartindent = true                        -- insert indents automatically
set.colorcolumn = "80"                        -- 
set.lazyredraw = true                         -- Won't be redrawn while executing macros, register and other commands.
set.relativenumber = true                     -- set relative numbered lines
set.swapfile = false                          -- creates a swapfile
set.backup = false                            -- creates a backup file
set.signcolumn = "yes"                        -- always show the sign column, otherwise it would shift the text each time
set.wrap = false                              -- display lines as one long line
set.timeoutlen = 500                          -- time to wait for a mapped sequence to complete (in milliseconds)
set.encoding = "utf-8"                        -- 
set.updatetime = 250                          -- time in milliseconds that language servers use to check for errors.
set.wildmenu = true                           -- shows a more advanced menu for autocomplete suggestions
set.title = true                              -- shows the title of the file
set.completeopt = "noinsert,menuone,noselect" -- behavior of the auto-complete menu
set.smartcase = true                          -- 

--
cmd[[colorscheme tokyonight]]

-- END
