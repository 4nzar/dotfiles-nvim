-- -------------------------------------------------------------------------- --
--                                                                            --
--       _   ___   ___   ___   ___   ___   ___   ___   ___   ___   ___        --
--       _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _|       --
--      |  _  |  _  |_   _|  _  |  _  |  _  |_   _|  _  |  _  |  _  |_        --
--      |_| |_| | |___| |___| | |_| |_| | |___| |___| | |_| |_| | |___|       --
--       _   _  |  ___   ___  |  _   _  |_   _____   _|  _   _  |  ___        --
--      | |_| | |_|  _| |_  |_| | |_| |  _| |_   _| |_  | |_| | |_|  _|       --
--      |_   _|  _  |_   _|  _  |_   _| |  _  | |  _  | |_   _|  _  |_        --
--       _| |___| |___| |___| |___| |_  |_| |_| |_| |_|  _| |___| |___|       --
--                                                                            --
--   plugins.lua                                                              --
--                                                                            --
--   By: 4nzar <sadik.essaidi@proton.me>                                      --
--                                                                            --
--   Created: 26/02/2023 22:07:11 by 4nzar                                    --
--   Updated: 03/03/2023 23:57:32 by 4nzar                                    --
--                                                                            --
-- -------------------------------------------------------------------------- --

-- START

local ensure_packer = function()
	local fn = vim.fn
	local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
	if fn.empty(fn.glob(install_path)) > 0 then
		fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
		vim.cmd [[packadd packer.nvim]]
		return true
	end
	return false
end

local packer_bootstrap = ensure_packer()

return require('packer').startup(function(use)
	use "wbthomason/packer.nvim"
	use "neovim/nvim-lspconfig"
	use "ray-x/lsp_signature.nvim"
	use { "L3MON4D3/LuaSnip", tag = "v<CurrentMajor>.*" }
	use { "https://gitlab.com/4nzar/header.nvim.git", as = "header.nvim" }
	use "hrsh7th/nvim-cmp"
	use "hrsh7th/cmp-nvim-lsp"
	use "nvim-lua/plenary.nvim"
	use "vimwiki/vimwiki"
	use "lervag/vimtex"
	use {
		'nvim-telescope/telescope.nvim', tag = '0.1.1',
		-- or                            , branch = '0.1.x',
		requires = { {'nvim-lua/plenary.nvim'} }
	}

	-- COLORSCHEME
	use "folke/tokyonight.nvim"

	-- Debugging
	use {
		"mfussenegger/nvim-dap",
		opt = true,
		event = "BufReadPre",
		module = { "dap" },
		config = function()
			require("config.dap").setup()
		end,
	}
	use "theHamsta/nvim-dap-virtual-text"
	use { "rcarriga/nvim-dap-ui", requires = {"mfussenegger/nvim-dap"} }
	use "nvim-telescope/telescope-dap.nvim"
	use "folke/which-key.nvim"
	use "jbyuki/one-small-step-for-vimkind"
	-- INSERT PACKAGE HERE
	use "leoluz/nvim-dap-go"

	-- Automatically set up your configuration after cloning packer.nvim
	-- Put this at the end after all plugins
	if packer_bootstrap then require('packer').sync() end
end)

-- END
