-- START
local cmd = vim.cmd
local g = vim.g

vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerCompile
  augroup end
]])

-- Commands
local create_cmd = vim.api.nvim_create_user_command

create_cmd('PackerInstall', function()
  cmd [[packadd packer.nvim]]
  require("plugins.plugins").install()
end, {})
create_cmd('PackerUpdate', function()
  cmd [[packadd packer.nvim]]
  require("plugins.plugins").update()
end, {})
create_cmd('PackerSync', function()
  cmd [[packadd packer.nvim]]
  require("plugins.plugins").sync()
end, {})
create_cmd('PackerClean', function()
  cmd [[packadd packer.nvim]]
  require("plugins.plugins").clean()
end, {})
create_cmd('PackerCompile', function()
  cmd [[packadd packer.nvim]]
  require("plugins.plugins").compile()
end, {})
-- END
